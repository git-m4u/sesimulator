FROM archlinux AS build
RUN pacman -Sy && \
    pacman --noconfirm -S cmake make gcc boost boost-libs
COPY . .
RUN cmake . && make

FROM archlinux
COPY --from=build SESimulator ./

ENTRYPOINT ["./SESimulator"]