#ifndef SESIMULATOR_TRANSACTION_H
#define SESIMULATOR_TRANSACTION_H

#include <utility>

#include "iso8583.h"

class Transaction {
public:
    Iso iso200;
    Iso iso210;

    Transaction() = default;

    Transaction(Iso iso200, Iso iso210) {
        Transaction::iso200 = std::move(iso200);
        Transaction::iso210 = std::move(iso210);
    }
};


#endif //SESIMULATOR_TRANSACTION_H
