#ifndef SESIMULATOR_BLOCKING_QUEUE_H
#define SESIMULATOR_BLOCKING_QUEUE_H

#include <mutex>
#include <queue>

template <typename T>
class BlockingQueue {
private:
    std::queue<T> queue;
    std::mutex mutex;

public:
    void push(const T& value);
    T pop();
    bool empty();
};

template<typename T>
void BlockingQueue<T>::push(const T &value) {
    const std::lock_guard<std::mutex> lock(mutex);
    queue.push(value);
}

template<typename T>
T BlockingQueue<T>::pop() {
    const std::lock_guard<std::mutex> lock(mutex);
    T value = queue.front();
    queue.pop();

    return value;
}

template<typename T>
bool BlockingQueue<T>::empty() {
    const std::lock_guard<std::mutex> lock(mutex);
    return queue.empty();
}

#endif
