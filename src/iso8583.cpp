#include "iso8583.h"

#include <iomanip>
#include <algorithm>

// Public

Iso::Iso(const std::string &iso) {
    setMTI(iso.substr(0, 4));
    setFields(iso);
}

void Iso::setMTI(std::string value) {
    Iso::mti = std::move(value);
}

void Iso::setField(unsigned int number, std::string value) {
    Iso::fields[number] = std::move(value);
}

std::string Iso::getMTI() {
    return mti;
}

std::string Iso::getField(unsigned int pos) const {
    return Iso::fields.at(pos);
}

std::string Iso::toString() {
    return Iso::getMTI() + Iso::buildBitmap() + Iso::buildData();
}

// Private

void Iso::setFields(const std::string &iso) {
    bool hasSecondBitmap = false;
    const std::vector<int> &bits = Iso::getBits(iso, hasSecondBitmap);
    int dataPos = 20, n = 0;

    if (hasSecondBitmap)
        dataPos += 16;

    std::string data = iso.substr(dataPos);

    for (int bit: bits) {
        int size;
        std::string value;

        switch (bit) {
            case 4:
                value = data.substr(n, 12);
                n += 12;
                break;
            case 7:
                value = data.substr(n, 10);
                n += 10;
                break;
            case 3:
            case 11:
            case 12:
                value = data.substr(n, 6);
                n += 6;
                break;
            case 13:
            case 15:
                value = data.substr(n, 4);
                n += 4;
                break;
            case 32:
                size = std::stoi(data.substr(n, 2));
                n += 2;
                value = data.substr(n, size);
                n += size;
                break;
            case 39:
                value = data.substr(n, 2);
                n += 2;
                break;
            case 41:
                value = data.substr(n, 8);
                n += 8;
                break;
            case 42:
                value = data.substr(n, 15);
                n += 15;
                break;
            case 61:
            case 62:
            case 127:
                size = std::stoi(data.substr(n, 3));
                n += 3;
                value = data.substr(n, size);
                n += size;
                break;

            default:
                continue;
        }

        setField(bit, value);
    }
}

std::vector<int> Iso::getBits(const std::string &iso, bool& hasSecondBitmap) {
    std::vector<int> bits;

    std::bitset<64> primary_bitmap = getBitmap(iso.substr(4, 16));

    for (int i = 63; i >= 0; i--) {
        if (primary_bitmap.test(i)) {
            bits.push_back(63 - i + 1);
        }
    }

    hasSecondBitmap = primary_bitmap.test(63);

    if (hasSecondBitmap) {
        std::bitset<64> second_bitmap = getBitmap(iso.substr(20, 16));

        for (int i = 63; i >= 0; i--) {
            if (second_bitmap.test(i)) {
                bits.push_back((63 - i + 1 + 64));
            }
        }
    }

    return bits;
}

std::bitset<64> Iso::getBitmap(const std::string &isoBitmap) {
    std::string bits;

    for (int i = 0; i < 16; i += 2) {
        std::stringstream ss;
        unsigned ui;
        ss << std::hex
           << isoBitmap.substr(i, 2);
        ss >> ui;
        bits += std::bitset<8>(ui).to_string();
        ss.clear();
    }

    return std::bitset<64>(bits);
}

std::string Iso::buildBitmap() {
    std::vector<std::bitset<64>> bitmaps;
    bitmaps.emplace_back();

    if (fields.rbegin()->first > 64) {
        bitmaps[0].set(0);
        bitmaps.emplace_back();
    }

    for (const auto&[key, value]: fields) {
        if (key > 64) bitmaps[1].set(key - 65);
        else bitmaps[0].set(key - 1);
    }

    std::string bitmap;

    for (auto &i: bitmaps) {
        std::string bits = i.to_string();
        std::reverse(bits.begin(), bits.end());
        std::ostringstream ss;

        for (int j = 0; j < 64; j += 8) {
            ss << std::setfill('0')
               << std::setw(2)
               << std::uppercase
               << std::hex
               << std::bitset<8>(bits.substr(j, 8)).to_ulong();
        }

        bitmap += ss.str();
        ss.clear();
    }

    return bitmap;
}

std::string Iso::buildData() {
    std::ostringstream data;

    for (const auto&[key, value]: fields) {
        if (key == 32) {
            data << std::setfill('0')
                 << std::setw(2)
                 << std::to_string(value.length());
        } else if (key == 61 || key == 62 || key == 127 || key == 125) {
            data << std::setfill('0')
                 << std::setw(3)
                 << std::to_string(value.length());
        }

        data << value;
    }

    return data.str();
}