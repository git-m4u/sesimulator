#ifndef SESIMULATOR_BLOCKING_MAP_H
#define SESIMULATOR_BLOCKING_MAP_H

#include <mutex>
#include <map>

template <typename K, typename T>
class BlockingMap {
private:
    std::map<K, T> map;
    std::mutex mutex;

public:
    void put(K key, const T& value);
    T get(K key);
    bool empty();
    void remove(K key);
    std::map<K, T> copy();
};

template <typename K, typename T>
void BlockingMap<K,T>::put(K key, const T &value) {
    const std::lock_guard<std::mutex> lock(mutex);
    map[key] = value;
}

template <typename K, typename T>
T BlockingMap<K,T>::get(K key) {
    const std::lock_guard<std::mutex> lock(mutex);
    return map.contains(key) ? map[key] : nullptr;
}

template <typename K, typename T>
bool BlockingMap<K,T>::empty() {
    const std::lock_guard<std::mutex> lock(mutex);
    return map.empty();
}

template <typename K, typename T>
void BlockingMap<K,T>::remove(K key) {
    const std::lock_guard<std::mutex> lock(mutex);

    if (map.empty())
        return;

    T value = map[key];
    map.erase(key);
}

template<typename K, typename T>
std::map<K, T> BlockingMap<K, T>::copy() {
    const std::lock_guard<std::mutex> lock(mutex);
    return std::map<K, T>(map);
}


#endif //SESIMULATOR_BLOCKING_MAP_H
