#include <iomanip>
#include <string>
#include <thread>
#include <chrono>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include <boost/log/trivial.hpp>

#include "iso8583.h"
#include "blocking_queue.h"
#include "blocking_map.h"
#include "transaction.h"

using namespace std;

void check_args(int argc, char *argv[]);
int init(const char *host, int port);
[[noreturn]] void simulateIso800();
[[noreturn]] void simulateIso600();
void readIso(int socket_handle);
[[noreturn]] void sendIso(int socket_handle);
void process(const string &iso);
string addHeader(string message);
Iso buildIso800();
Iso buildIso210(const string &iso200);
Iso buildIso600(const Transaction &transaction);

BlockingQueue<std::string> isos;
BlockingMap<std::string, Transaction> transactions;

int main(int argc, char *argv[]) {
    check_args(argc, argv);
    int socket_handle = init(argv[1], stoi(argv[2]));

    thread reader(readIso, socket_handle);
    thread sender(sendIso, socket_handle);
    thread simulate800(simulateIso800);
    thread simulate600(simulateIso600);

    reader.join();

    return 0;
}

void check_args(int argc, char *argv[]) {
    if (argc == 1 || argc > 3) {
        BOOST_LOG_TRIVIAL(info) << "Invalid args. Usage: SESimulator <host> <port>";
        exit(-1);
    }

    BOOST_LOG_TRIVIAL(info) << "Host: " << argv[1];
    BOOST_LOG_TRIVIAL(info) << "Port: " << argv[2];
}

int init(const char *host, int port) {
    int socket_handle;
    struct sockaddr_in server_addr{};

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);

    socket_handle = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_handle < 0) {
        BOOST_LOG_TRIVIAL(error) << "socket failed";
        std::exit(-2);
    }

    struct hostent *he = gethostbyname(host);
    if (he == nullptr) {
        BOOST_LOG_TRIVIAL(error) << "gethostbyname failed";
        std::exit(-3);
    }

    if (inet_pton(AF_INET, inet_ntoa(*(struct in_addr*)he->h_addr), &server_addr.sin_addr) <= 0) {
        BOOST_LOG_TRIVIAL(error) << "inet_pton failed";
        std::exit(-4);
    }

    if (connect(socket_handle, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        BOOST_LOG_TRIVIAL(error) << "connect failed";
        std::exit(-5);
    }

    return socket_handle;
}

string addHeader(string message) {
    stringstream header;
    header << setfill('0')
           << setw(4)
           << to_string(message.length());
    message = header.str() + message;

    return message;
}

Iso buildIso800() {
    Iso iso800;
    iso800.setMTI("0800");
    iso800.setField(7, "0726123308");
    iso800.setField(11, "999999");
    iso800.setField(12, "123308");
    iso800.setField(13, "0726");

    return iso800;
}

Iso buildIso210(const string &iso200) {
    Iso iso210 = Iso(iso200);
    iso210.setMTI("0210");
    iso210.setField(39, "00"); // TODO: modificar o status code...
    iso210.setField(62,"FA OP confirma sua Recarga no valor de@R$ XX,XX validos por XX dias.@Para consultar seu saldo disque *222#.@Para saldo promocional disque *767#");
    iso210.setField(127, "000131541"); // TODO: gerar o NSU host...

    return iso210;
}

Iso buildIso600(const Transaction &transaction)
{
    Iso iso600;
    iso600.setMTI("0600");
    iso600.setField(7, transaction.iso200.getField(7));
    iso600.setField(11, transaction.iso200.getField(11));
    iso600.setField(12, transaction.iso210.getField(12));
    iso600.setField(13, transaction.iso210.getField(13));
    iso600.setField(32, transaction.iso200.getField(32));
    iso600.setField(41, transaction.iso210.getField(41));
    iso600.setField(42, transaction.iso200.getField(42));
    iso600.setField(125, transaction.iso200.getField(11));
    iso600.setField(127, transaction.iso210.getField(127));

    return iso600;
}

void readIso(int socket_handle) {
    while (true) {
        char buffer[9999] = {0}, header[4] = {0};
        unsigned int headerSize;
        if (read(socket_handle, header, 4) <= 0) continue;
        headerSize = atoi(header);
        if (read(socket_handle, buffer, headerSize) <= 0) break;
        BOOST_LOG_TRIVIAL(info) << "Receveid: " << buffer;
        process(string(buffer));
    }
}

[[noreturn]] void sendIso(int socket_handle) {
    while (true) {
        if (isos.empty()) continue;
        string message = isos.pop();
        send(socket_handle, message.c_str(), message.length(), 0);
        BOOST_LOG_TRIVIAL(info) << "Sent: " << message;
    }
}

[[noreturn]] void simulateIso800() {
    while (true) {
        isos.push(addHeader(buildIso800().toString()));
        std::this_thread::sleep_for(std::chrono::milliseconds(60000));
    }
}

[[noreturn]] void simulateIso600() {
    while (true) {
        if (!transactions.empty()) {
            for (const auto&[key, value]: transactions.copy()) {
                isos.push(addHeader(buildIso600(value).toString()));
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(60000));
    }
}

void process(const string &iso) {
    std::string type = iso.substr(0, 4);
    if ("0200" == type) {
        Iso iso200 = Iso(iso);
        Iso iso210 = buildIso210(iso);
        Transaction transaction(iso200, iso210);
        transactions.put(iso210.getField(11), transaction);
        isos.push(addHeader(iso210.toString()));
    } else if ("0202" == type) {
        Iso iso202 = Iso(iso);
        transactions.remove(iso202.getField(11));
    } else if ("9200" == type) {
        // TODO
    }
}