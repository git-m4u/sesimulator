#ifndef SESIMULATOR_ISO8583_H
#define SESIMULATOR_ISO8583_H

#include <map>
#include <string>
#include <bitset>
#include <vector>

class Iso {
private:
    std::string mti;
    std::map<unsigned int, std::string> fields;

    std::string buildBitmap();
    std::string buildData();
    std::vector<int> getBits(const std::string &iso, bool&hasSecondBitmap) ;
    void setFields(const std::string &iso);
    std::bitset<64> getBitmap(const std::string &isoBitmap);

public:
    Iso() = default;
    explicit Iso(const std::string &iso);

    void setMTI(std::string value);
    void setField(unsigned int number, std::string value);
    std::string getMTI();
    [[nodiscard]] std::string getField(unsigned int pos) const;
    std::string toString();
};

#endif
