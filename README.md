# SESimulator
É um simples projeto para simular a Software Express

## Build
```bash
docker image build -t sesimulator .
```
## Executando
```bash
docker run -it sesimulator <host> <port>
```